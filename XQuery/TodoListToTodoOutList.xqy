xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.example.org";
(:: import schema at "../Xsd/OutOne.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/top/dbReference";
(:: import schema at "../Resources/dbReference_table.xsd" ::)

declare variable $TodoList as element() (:: schema-element(ns1:TodoCollection) ::) external;

declare function local:TodoListToTodoOutList($TodoList as element() (:: schema-element(ns1:TodoCollection) ::)) as element() (:: schema-element(ns2:TodoOutList) ::) {
    <ns2:TodoOutList>
        {
            for $Todo in $TodoList/ns1:Todo
            return 
            
                for $Todo1 in $Todo
                return 
                <ns2:TodoOut>
                    <ns2:id>{fn:data($Todo/ns1:id)}</ns2:id>
                    <ns2:Resume>{fn:concat(fn:substring-before($Todo/ns1:descripcion, fn:substring($Todo/ns1:descripcion, 5)), $Todo/ns1:titulo)}</ns2:Resume>
                   
                    <ns2:Puntuation>{fn:sum($Todo/ns1:puntuacion, xs:double(fn:data($Todo/ns1:prioridad)))}</ns2:Puntuation></ns2:TodoOut>
        }
    </ns2:TodoOutList>
};

local:TodoListToTodoOutList($TodoList)
