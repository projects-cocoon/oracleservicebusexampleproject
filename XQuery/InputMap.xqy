xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.example.org";
(:: import schema at "../Xsd/OutOne.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/top/dbReference";
(:: import schema at "../Resources/dbReference_table.xsd" ::)

declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";

declare variable $InputData as element() (:: schema-element(ns1:TodoPrimaryKey) ::) external;

declare function local:InputMap($InputData as element() (:: schema-element(ns1:TodoPrimaryKey) ::)) as element() (:: schema-element(ns2:Request) ::) {

      <ns2:Request>
          <ns2:id>{fn:data($InputData/ns1:id)}</ns2:id>
      </ns2:Request>

};

local:InputMap($InputData)
