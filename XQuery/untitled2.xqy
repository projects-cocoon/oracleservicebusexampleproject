xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.example.org";
(:: import schema at "../Xsd/OutOne.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/top/dbReference";
(:: import schema at "../Resources/dbReference_table.xsd" ::)

declare variable $aaa as element() (:: schema-element(ns1:TodoCollection) ::) external;

declare function local:func($aaa as element() (:: schema-element(ns1:TodoCollection) ::)) as element() (:: schema-element(ns2:TodoOut) ::) {
    <ns2:TodoOut/>
};

local:func($aaa)
