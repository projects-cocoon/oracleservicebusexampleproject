xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.myschema.org";
(:: import schema at "../Xsd/MySchema.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/top/dbReference";
(:: import schema at "../Resources/dbReference_table.xsd" ::)

declare variable $InputData as element() (:: schema-element(ns1:Todo) ::) external;

declare function local:MapFunction($InputData as element() (:: schema-element(ns1:Todo) ::)) as element() (:: schema-element(ns2:TodoCollection) ::) {
    <ns2:TodoCollection>
        <ns2:Todo>
            <ns2:id>{fn:data($InputData/ns1:id)}</ns2:id>
            {
                if ($InputData/ns1:titulo and $InputData/ns1:descripcion)
                then <ns2:titulo>{fn:concat(fn:data($InputData/ns1:titulo), $InputData/ns1:descripcion)}</ns2:titulo>
                else if($InputData/ns1:titulo)then <ns2:descripcion>No Title</ns2:descripcion>
                else (
                 <ns2:titulo>No data found</ns2:titulo>
                 )
            }
            <ns2:descripcion>ZZZZZZ</ns2:descripcion>
            {
                if ($InputData/ns1:puntuacion)
                then <ns2:puntuacion>{fn:data($InputData/ns1:puntuacion)}</ns2:puntuacion>
                else ()
            }
            {
                if ($InputData/ns1:prioridad)
                then <ns2:prioridad>{fn:data($InputData/ns1:prioridad)}</ns2:prioridad>
                else ()
            }
        </ns2:Todo>
    </ns2:TodoCollection>
};

local:MapFunction($InputData)
