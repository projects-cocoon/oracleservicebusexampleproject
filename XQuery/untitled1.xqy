xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.example.org";
(:: import schema at "../Xsd/OutOne.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/top/dbReference";
(:: import schema at "../Resources/dbReference_table.xsd" ::)

declare variable $dddd as element() (:: schema-element(ns1:Todo) ::) external;

declare function local:func($dddd as element() (:: schema-element(ns1:Todo) ::)) as element() (:: schema-element(ns2:TodoOut) ::) {
    <ns2:TodoOut>
        <ns2:id>{fn:data($dddd/ns1:id)}</ns2:id>
        <ns2:Resume>{fn:concat("Resume: ", fn:data($dddd/ns1:titulo), " ", $dddd/ns1:descripcion)}</ns2:Resume>
        <ns2:Puntuation>{fn:sum(3,1)}</ns2:Puntuation>
    </ns2:TodoOut>
};

local:func($dddd)
